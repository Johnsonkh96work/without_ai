package com.example.without_ai.ui

import com.example.without_ai.data.Movie

data class StateList(
    val isLoading: Boolean = false,
    val data: List<Movie>? = null,
    val error: String? = null,
)