package com.example.without_ai.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.without_ai.R
import com.example.without_ai.data.Movie
import com.example.without_ai.utils.MOVIE_DETAILS

class InfoAdapter(
    private var infoList: ArrayList<Movie>,
) : RecyclerView.Adapter<InfoAdapter.DataViewHolder>() {

    class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(movie: Movie) {
            itemView.findViewById<TextView>(R.id.name_and_price).text =
                movie.name + " " + movie.price
            itemView.setOnClickListener {
                it.findNavController()
                    .navigate(
                        R.id.action_FirstFragment_to_SecondFragment,
                        bundleOf(MOVIE_DETAILS to movie)
                    )
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DataViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_info, parent,
                false
            )
        )

    override fun getItemCount(): Int = infoList.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) =
        holder.bind(infoList[position])

    fun updateData(list: List<Movie>) {
        infoList = arrayListOf()
        infoList.addAll(list)
    }

}