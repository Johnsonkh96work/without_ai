package com.example.without_ai.ui

import com.example.without_ai.data.MovieDetails

data class StateDetails(
    val isLoading: Boolean = false,
    val data: MovieDetails? = null,
    val error: String? = null,
)