package com.example.without_ai

import android.app.Application
import com.example.without_ai.data.MainRepository
import com.example.without_ai.data.RetrofitService

class MyApplication : Application() {

    val mainRepository = MainRepository(RetrofitService.getInstance())
}
