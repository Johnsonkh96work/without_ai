package com.example.without_ai

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.example.without_ai.data.MainRepository
import com.example.without_ai.ui.StateDetails
import com.example.without_ai.ui.StateList
import kotlinx.coroutines.launch

class MainViewModel(
    private val repository: MainRepository,
) : ViewModel() {

    private val _stateList = MutableLiveData(StateList(isLoading = false))
    val stateList: LiveData<StateList>
        get() = _stateList

    private val _stateDetails = MutableLiveData(StateDetails(isLoading = false))
    val stateDetails: LiveData<StateDetails>
        get() = _stateDetails

    companion object {
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val mainRepository = (this[APPLICATION_KEY] as MyApplication).mainRepository
                MainViewModel(
                    mainRepository
                )
            }
        }
    }

    fun fetchMovies() {
        viewModelScope.launch {
            try {
                _stateList.value = _stateList.value?.copy(isLoading = true)
                val movieList = repository.getMovies()
                _stateList.postValue(_stateList.value?.copy(data = movieList, isLoading = false))
            } catch (e: Exception) {
                _stateList.value?.copy(error = (e.localizedMessage), isLoading = false)
            }
        }
    }

    fun fetchMovieBy(id: String) {
        viewModelScope.launch {
            try {
                _stateDetails.value = _stateDetails.value?.copy(isLoading = true)
                val movie = repository.getMovieDetailsById(id)
                _stateDetails.postValue(_stateDetails.value?.copy(data = movie, isLoading = false))
            } catch (e: Exception) {
                _stateDetails.value?.copy(error = (e.localizedMessage), isLoading = false)
            }
        }
    }
}