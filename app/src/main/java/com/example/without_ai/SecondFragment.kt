package com.example.without_ai

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import com.bumptech.glide.Glide
import com.example.without_ai.data.Movie
import com.example.without_ai.databinding.FragmentSecondBinding
import com.example.without_ai.utils.MOVIE_DETAILS

class SecondFragment : Fragment() {

    private var _binding: FragmentSecondBinding? = null
    private val binding get() = _binding!!
    private val viewModel: MainViewModel by activityViewModels { MainViewModel.Factory }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val movie = arguments?.getParcelable<Movie>(MOVIE_DETAILS)
        observeViewModel()
        viewModel.fetchMovieBy(movie?.id.orEmpty())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun observeViewModel() {
        viewModel.stateDetails.observe(viewLifecycleOwner) {
            if (!it.isLoading) {
                it.data?.let { movieDetails ->
                    binding.nameAndPrice.text = movieDetails.name + " " + movieDetails.price
                    binding.meta.text = movieDetails.meta
                    binding.rating.text = movieDetails.rating
                    binding.synopsis.text = movieDetails.synopsis
                    Glide.with(binding.root)
                        .load(movieDetails.image)
                        .into(binding.image)
                }
            }
            if (it.error != null) {
                Toast.makeText(activity, it.error, Toast.LENGTH_LONG).show()
            }
        }
    }
}