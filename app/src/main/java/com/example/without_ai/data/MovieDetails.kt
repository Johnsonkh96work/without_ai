package com.example.without_ai.data

import android.os.Parcelable
import com.example.without_ai.utils.EMPTY_STRING
import com.example.without_ai.utils.ZERO
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class MovieDetails(
    @SerializedName("image") val image: String = EMPTY_STRING,
    @SerializedName("meta") val meta: String = EMPTY_STRING,
    @SerializedName("name") val name: String = EMPTY_STRING,
    @SerializedName("price") val price: Int = ZERO,
    @SerializedName("rating") val rating: String = EMPTY_STRING,
    @SerializedName("synopsis") val synopsis: String = EMPTY_STRING,
) : Parcelable
