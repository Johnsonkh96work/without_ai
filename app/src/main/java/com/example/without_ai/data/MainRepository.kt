package com.example.without_ai.data

class MainRepository constructor(private val retrofitService: RetrofitService) {

    suspend fun getMovies() = retrofitService.getAllMovies().body() ?: emptyList()

    suspend fun getMovieDetailsById(id: String) = retrofitService.getMovieDetailsById(id).body()?: MovieDetails()

}