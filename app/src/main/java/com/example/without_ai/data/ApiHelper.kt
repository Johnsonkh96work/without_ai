package com.example.without_ai.data

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitService {

    companion object {

        private const val BASE_URL = "https://us-central1-temporary-692af.cloudfunctions.net"

        var retrofitService: RetrofitService? = null
        fun getInstance(): RetrofitService {
            if (retrofitService == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                retrofitService = retrofit.create(RetrofitService::class.java)
            }
            return retrofitService!!
        }
    }

    @GET("/movies")
    suspend fun getAllMovies(): Response<List<Movie>>

    @GET("/movieDetails")
    suspend fun getMovieDetailsById(@Query("id") id: String): Response<MovieDetails>

}