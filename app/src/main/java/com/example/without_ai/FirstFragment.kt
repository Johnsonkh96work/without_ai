package com.example.without_ai

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.without_ai.data.Movie
import com.example.without_ai.ui.InfoAdapter
import com.example.without_ai.databinding.FragmentFirstBinding

class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!
    private var adapter: InfoAdapter = InfoAdapter(arrayListOf())
    private val viewModel: MainViewModel by activityViewModels { MainViewModel.Factory }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
        observeViewModel()
        viewModel.fetchMovies()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupUI() {
        binding.let {
            it.recyclerView.layoutManager = LinearLayoutManager(activity)
            it.recyclerView.run {
                addItemDecoration(
                    DividerItemDecoration(
                        it.recyclerView.context,
                        (it.recyclerView.layoutManager as LinearLayoutManager).orientation
                    )
                )
            }
            it.recyclerView.adapter = adapter
        }
    }

    private fun observeViewModel() {
        viewModel.stateList.observe(viewLifecycleOwner) {
            if (it.isLoading) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
                it.data?.let { data -> renderList(data) }
            }
            if (it.error != null) {
                binding.progressBar.visibility = View.GONE
                Toast.makeText(activity, it.error, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun renderList(users: List<Movie>) {
        binding.recyclerView.visibility = View.VISIBLE
        users.let { listOfMovie -> listOfMovie.let { adapter.updateData(it) } }
    }
}